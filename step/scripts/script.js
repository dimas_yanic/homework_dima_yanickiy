////////////////////////////////OUR SERVICES/////////////////////////////////

let ourServicesBtn = document.querySelectorAll('.service-button');
let description = document.querySelectorAll('.service-description');

for(let j = 0; j < ourServicesBtn.length; j++){
    ourServicesBtn[j].addEventListener('click', () => {
        for(let i of description){
            i.classList.remove('visible');
        }
        for(let i of ourServicesBtn){
            i.classList.remove('active');
        }
        ourServicesBtn[j].classList.add('active');
        description[j].classList.add('visible');
    });
}

////////////////////////////////OUR AMAZING WORK/////////////////////////////////

let ourAmazingWorkCards = document.querySelectorAll('.work-card');
let workCardImg = document.querySelectorAll('.work-img');
let workCardRevers = document.querySelectorAll('.work-card-revers');

for(let i = 0; i < ourAmazingWorkCards.length; i++){
    ourAmazingWorkCards[i].addEventListener('mouseover', () => {
        workCardImg[i].classList.add('disabled');
        workCardRevers[i].classList.remove('disabled');
    });
    ourAmazingWorkCards[i].addEventListener('mouseout', () => {
        workCardImg[i].classList.remove('disabled');
        workCardRevers[i].classList.add('disabled');
    });
}

let visibleImages = 12;
let workButtons = document.querySelectorAll('.work-button');
let workCards = document.querySelectorAll('.work-card');
let btnLoadMore = document.querySelector('.load-button');

loadMore();
btnLoadMore.addEventListener('click', ()=>{
    if(visibleImages < 36){
        visibleImages+=12;
        loadMore();
    }
    if(visibleImages >= 36){
        btnLoadMore.classList.add('disabled');
    }
});

for(let i = 0; i < workButtons.length; i++){
    workButtons[i].addEventListener('click', ()=>{
        for(let i = 0; i < workButtons.length; i++){
            workButtons[i].classList.remove('all');
        }
        workButtons[i].classList.add('all');
       
        if(workButtons[i].id === 'graphicDesign'){
            for(let i = 0; i < 36; i++){
                workCards[i].classList.add('disabled');
            }
            for(let i = 0; i < visibleImages; i++){
                if(workCards[i].id === 'graphicDesign') {
                    workCards[i].classList.remove('disabled');
                }
            }
        } else if(workButtons[i].id === 'webDesign'){
            for(let i = 0; i < 36; i++){
                workCards[i].classList.add('disabled');
            }
            for(let i = 0; i < visibleImages; i++){
                if(workCards[i].id === 'webDesign') {
                    workCards[i].classList.remove('disabled');
                }
            }
        } else if(workButtons[i].id === 'landingPage'){
            for(let i = 0; i < 36; i++){
                workCards[i].classList.add('disabled');
            }
            for(let i = 0; i < visibleImages; i++){
                if(workCards[i].id === 'landingPage') {
                    workCards[i].classList.remove('disabled');
                }
            }
        } else if(workButtons[i].id === 'wordpress'){
            for(let i = 0; i < 36; i++){
                workCards[i].classList.add('disabled');
            }
            for(let i = 0; i < visibleImages; i++){
                if(workCards[i].id === 'wordpress') {
                    workCards[i].classList.remove('disabled');
                }
            }
        } else {
            loadMore();
        }
    });
}

function loadMore(){
    for(let i = 0; i < 36; i++){
        workCards[i].classList.add('disabled');
    }
    for(let i = 0; i < visibleImages; i++){
        workCards[i].classList.remove('disabled');
    } 
}

////////////////////////////////Breaking News/////////////////////////////////

let newsCards = document.querySelectorAll('.news-card');
let newsDate = document.querySelectorAll('.date');
let h = document.querySelectorAll('.h');

for(let i = 0; i < newsCards.length; i++){
    newsCards[i].addEventListener('mouseover', () => {
        newsDate[i].style.background = '#18CFAB';
        h[i].style.color = '#18CFAB';
        newsDate[i].style.transition = 'all 0.2s';
        h[i].style.transition = 'all 0.2s';
    });
    newsCards[i].addEventListener('mouseout', () => {
        newsDate[i].style.background = '#203E38';
        h[i].style.color = '#717171';
        newsDate[i].style.transition = 'all 0.2s';
        h[i].style.transition = 'all 0.2s';
    });
}

////////////////////////////////What People Say About theHam/////////////////////////////////

let comment = document.querySelector('.people-say');
let name = document.querySelector('.name');
let working = document.querySelector('.working');
let photo = document.querySelectorAll('.icon-big');
let smallPhotos = document.querySelectorAll('.icon-small');
let leftArrow = document.querySelector('.left-arrow');
let rightArrow = document.querySelector('.right-arrow');

let index = 2;
let comments = ["Lorem ipsum sd;kfvm ksdfjshy hbsdfgsuijsf suyfsb susbfbsj",
    "djhjsghfjhsjal jdsfhkgjyusdcy vsudytjvsyud sduukvsduksdhv",
    "Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.",
    "Integer , augue  ultricies , quam  laoreet , non  odio  quis . Morbi  odio  aliquam . Tempus  luctus,  dui  sem,  dictum  nisi  massa.  pulvinar  eget  facilisis."
];
let names = ["Lisa Simpson", "George Bruno", "Hasan Ali", "Anna Lopes"];
let works = ["Graphic Designer", "3D Designer", "UX Designer", "Web Designer"];

leftArrow.addEventListener('click', ()=>{
    if(index < 1){
        index = 3;
    } else {
        index--;
    }
    update();
});
rightArrow.addEventListener('click', ()=>{
    if(index > 2){
        index = 0;
    } else {
        index++;
    }
    update();
});
for(let i = 0; i < smallPhotos.length; i++){
    smallPhotos[i].addEventListener('click', ()=>{
        index = i;
        update();
    });
}

function update(){
    comment.innerText = comments[index];
    name.innerText = names[index];
    working.innerText = works[index];
    for(let i = 0; i < photo.length; i++){
        photo[i].classList.add('disabled');
        smallPhotos[i].classList.remove('enabled');
        smallPhotos[i].style.transition = "all 0.2s";
    }
    photo[index].classList.remove('disabled');
    setTimeout(()=>{
        smallPhotos[index].classList.add('enabled').style.transition = "all 0.5s";
    }, 201);
    
}