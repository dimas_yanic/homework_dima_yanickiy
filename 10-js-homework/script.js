let icons = document.querySelectorAll('.fa-eye');
let iconsDisabled = document.querySelectorAll('.fa-eye-slash');
let inputs = document.querySelectorAll('input');
let btn = document.querySelector('.btn');

for(let i = 0; i < icons.length; i++){
    icons[i].addEventListener('mousedown', () => {
        inputs[i].type = 'text';
        icons[i].style.visibility = 'hidden';
        iconsDisabled[i].classList.remove('disabled');
    });
    iconsDisabled[i].addEventListener('mouseup', () => {
        inputs[i].type = 'password';
        icons[i].style.visibility = 'visible';
        iconsDisabled[i].classList.add('disabled');
    });
}

btn.addEventListener('click', () => {
    let pass1 = inputs[0].value;
    let pass2 = inputs[1].value;
    if(pass1 === pass2){
        alert('You are welcome');
    } else {
        let form = document.querySelector('form');
        let text = document.createElement('p');
        text.innerText = 'Нужно ввести одинаковые значения';
        text.style.color = "#FF0000";
        btn.before(text);
        setInterval(() => {form.removeChild(text);}, 3000);
    }
});

