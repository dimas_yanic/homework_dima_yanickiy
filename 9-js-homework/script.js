let tabsTitle = document.querySelectorAll('.tabs-title');
let tabsContent = document.querySelector('.tabs-content');
let contents = tabsContent.querySelectorAll('li');

for(let i = 0; i < tabsTitle.length; i++){
    tabsTitle[i].addEventListener('click', () => {
        for(let item of tabsTitle){
            item.classList.remove('active');
        }
        for(let content of contents){
            content.classList.add('disabled');
        }

        event.currentTarget.classList.add('active');
        contents[i].classList.remove('disabled');
    });
}

