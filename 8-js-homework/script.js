let input = document.createElement('input');
let incorrectNumber;
document.body.appendChild(input);

input.addEventListener('focusin', () => {
    input.style.borderColor = '#00C322';
    input.style.borderWidth = '4px';
    document.body.removeChild(incorrectNumber);
});
input.addEventListener('focusout', () => {
    input.style.borderColor = '#000000';
    input.style.borderWidth = '1px';

    let num = parseInt(input.value);
    if(!isNaN(num) && num > 0){
        let span = document.createElement('span');
        span.classList.add('label');
        span.innerText = `Текущая цена: ${num.toString()}`;
        document.body.append(span);

        let container = document.createElement("div");
        container.classList.add('container');
        span.append(container);

        container.addEventListener('click', () => {
            document.body.removeChild(span);
            input.innerText = '';
        });
    } else{
        incorrectNumber = document.createElement('p');
        incorrectNumber.innerText = 'Please enter correct price';
        input.style.borderColor = '#FF0000';
        input.style.borderWidth = '4px';
        document.body.append(incorrectNumber);
    }
});


